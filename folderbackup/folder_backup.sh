#!/bin/bash


########################################################################
#                                                                      #
#                       F O L D E R B A C K U P                        #
#                                                                      #
#              BACKUP AND COMPRESS FOLDERS IN THE WEBROOT              #
#                                                                      #
#                      Copyright 2017 by PB-Soft                       #
#                                                                      #
#                           www.pb-soft.com                            #
#                                                                      #
#                       Version 1.5 / 03.06.2017                       #
#                                                                      #
########################################################################
#
# Check that the following software packages are installed on your
# system if you want to send emails:
#
#  - sendemail (lightweight, command line SMTP email client)
#
#  - libnet-ssleay-perl (perl module to call Secure Sockets Layer (SSL)
#                       functions of the SSLeay library)
#
#  - libio-socket-ssl-perl (perl module that uses SSL to encrypt data)
#
# ======================================================================
#
# You can use the following commands to install the packages:
#
#   apt-get install sendemail
#
#   apt-get install libnet-ssleay-perl
#
#   apt-get install libio-socket-ssl-perl
#
# ======================================================================
#
# Check the following link for information about the sendEmail tool:
#
#   http://caspian.dotconf.net/menu/Software/SendEmail/
#
# ======================================================================
#
# Check that the following two firewall ports are open and only allow
# CIFS (Common Internet File System) file access from the localhost
# (where this script is running) to the external filesystem (NAS inside
# the local LAN network)! Do NOT open the port 445 to the WAN or other
# untrusted networks!
#
#   Port 445 - Common Internet File System (CIFS) => file access
#
#   Port 587 - Simple Mail Transfer Protocol (SMTP) => sending emails
#
# ======================================================================


########################################################################
########################################################################
##############   C O N F I G U R A T I O N   B E G I N   ###############
########################################################################
########################################################################


# ======================================================================
# Specify the path of the backup root directory.
# ======================================================================
BACKUP_ROOTDIR="/var/www"


# ======================================================================
# Specify if the files in the webroot folder should be copied (yes/no).
# ======================================================================
BACKUP_WEBROOT_FILES="yes"


# ======================================================================
# Specify the names of the subdirectories which have to be included in
# the backup.
# ======================================================================
BACKUP_SUBDIRS="directory1 directory2 directory3 directory4"


# ======================================================================
# Specify the local backup destination (directory).
# BE SURE TO PROTECT THIS DIRECTORY AGAINST EXTERNAL ACCESS !!!!
# ======================================================================
BACKUP_TARGET="/var/www/backup"


# ======================================================================
# Specify the HTTPS link to the local backup destination (directory).
# ======================================================================
BACKUP_LINK="https://example.com/backup"


# ======================================================================
# Specify how many backups should be stored locally. The backups will
# be stored in positions and the position 1 holds always the newest
# backup. The older the backup the higher the position (number).
# ======================================================================
BACKUP_STORE=6


# ======================================================================
# Specify if the backup archive should be copied to an external
# location like to a filesystem on a NAS (yes/no).
# ======================================================================
EXT_BACKUP="yes"


# ======================================================================
# Specify the username to access the external filesystem (NAS).
# ======================================================================
EXT_USERNAME="nas_username"


# ======================================================================
# Specify the password to access the external filesystem (NAS).
# ======================================================================
EXT_PASSWORD="nas_password"


# ======================================================================
# Specify IP address of the external backup host (NAS in the LAN).
# ======================================================================
EXT_IP="192.168.10.5"


# ======================================================================
# Specify the external share which has to be mounted (NAS).
# ======================================================================
EXT_SHARE="Backup/myhost"


# ======================================================================
# Specify the mount point for the external backup.
# ======================================================================
EXT_MOUNT_POINT="/mnt/backup"


# ======================================================================
# Specify if email notifications should be sent (yes/no).
# ======================================================================
EMAIL_NOTIFICATIONS="yes"


# ======================================================================
# Specify the connection details for the SMTP host.
# ======================================================================
EMAIL_USERNAME="john.doe@gmail.com"
EMAIL_PASSWORD="smtp_password"
EMAIL_HOSTNAME="smtp.gmail.com"
EMAIL_PORT="587"
EMAIL_TLS="yes"


# ======================================================================
# Specify the email sender.
# ======================================================================
EMAIL_SENDER="admin@example.com"


# ======================================================================
# Specify the email receiver(s). Multiple receivers may be specified by
# separating them by either a white space, comma, or semi-colon like:
#
#  EMAIL_RECEIVER="john@example.com brad@domain.com"
#
# ======================================================================
EMAIL_RECEIVER="john.doe@yahoo.com"


########################################################################
########################################################################
################   C O N F I G U R A T I O N   E N D   #################
########################################################################
########################################################################


# ======================================================================
# Clear the screen.
# ======================================================================
clear


# ======================================================================
# Specify the start time.
# ======================================================================
START=`date +%s`


# ======================================================================
# Specify the name of the directory path for the webroot files.
# ======================================================================
WEB_ROOTDIR="_root_"


# ======================================================================
# Get the paths of the necessary executable.
# ======================================================================
SENDEMAIL="$(which sendEmail)"
TAR="$(which tar)"


# ======================================================================
# Get the actual hostname where the script is running.
# ======================================================================
SCRIPT_HOST="$(hostname)"


# ======================================================================
# Check if the target backup directory does not exist.
# ======================================================================
if [ ! -d $BACKUP_TARGET ]; then


  # ====================================================================
  # Display some information.
  # ====================================================================
  echo
  echo "Create the target backup directory..."


  # ====================================================================
  # Create the target backup directory.
  # ====================================================================
  mkdir -p $BACKUP_TARGET
fi


# ======================================================================
# Check if the directory for the webroot files does not exist.
# ======================================================================
if [ ! -d $BACKUP_ROOTDIR/$WEB_ROOTDIR ]; then


  # ====================================================================
  # Display some information.
  # ====================================================================
  echo
  echo "Create the directory for the webroot files..."


  # ====================================================================
  # Create the directory for the webroot files.
  # ====================================================================
  mkdir -p $BACKUP_ROOTDIR/$WEB_ROOTDIR
fi


# ======================================================================
# Specify the actual date in the yyyy-mm-dd format. A_DATE for the file-
# name and E_DATE for the email.
# ======================================================================
A_DATE="$(date +"%Y-%m-%d")"
E_DATE="$(date +"%d.%m.%Y")"


# ======================================================================
# Specify the actual time in the hh-mm-ss format. A_TIME for the file-
# name and E_TIME for the email.
# ======================================================================
A_TIME="$(date +"%H-%M-%S")"
E_TIME="$(date +"%H:%M:%S")"


# ======================================================================
# Specify the backup base name.
# ======================================================================
BACKUP_BASENAME="Backup_$SCRIPT_HOST.tar.gz"


# ======================================================================
# Specify the backup full name.
# ======================================================================
BACKUP_FULLNAME="${A_DATE}_${A_TIME}_$BACKUP_BASENAME"


# ======================================================================
# Specify the backup path.
# ======================================================================
BACKUP_PATH="$BACKUP_TARGET/$BACKUP_FULLNAME"


########################################################################
########################################################################
###################   B A C K U P   F O L D E R S   ####################
########################################################################
########################################################################


# ======================================================================
# Check if the files in the webroot should be copied.
# ======================================================================
if [ "$BACKUP_WEBROOT_FILES" == "yes" ]; then


  # ====================================================================
  # Add the special folder to the subdirectory string.
  # ====================================================================
  BACKUP_SUBDIRS="$BACKUP_SUBDIRS $WEB_ROOTDIR"
fi


# ======================================================================
# Check if the backup folder string is not empty.
# ======================================================================
if [ "$BACKUP_SUBDIRS" != "" ]; then


  # ====================================================================
  # Change to the backup root directory.
  # ====================================================================
  cd $BACKUP_ROOTDIR


  # ====================================================================
  # Check if the files in the webroot should be copied.
  # ====================================================================
  if [ "$BACKUP_WEBROOT_FILES" == "yes" ]; then


    # ==================================================================
    # Display some information.
    # ==================================================================
    echo
    echo "Copying files from webroot directory..."


    # ==================================================================
    # Copy all files from the webroot directory to the special folder.
    # ==================================================================
    cp -u $BACKUP_ROOTDIR/* $BACKUP_ROOTDIR/$WEB_ROOTDIR 2>/dev/null
  fi


  # ====================================================================
  # Display some information.
  # ====================================================================
  echo
  echo "Backup webroot directories..."


  # ====================================================================
  # Backup all the folders into a compressed archive (tar.gz).
  # ====================================================================
  $TAR -czf $BACKUP_PATH $BACKUP_SUBDIRS


  ######################################################################
  ######################################################################
  ##########        M O U N T   E X T E R N A L   F S        ###########
  ######################################################################
  ######################################################################


  # ====================================================================
  # Check if the backup should be copied to an external location.
  # ====================================================================
  if [ "$EXT_BACKUP" == "yes" ]; then


    # ==================================================================
    # Check if the mountpoint does not exist.
    # ==================================================================
    if [ ! -d "$EXT_MOUNT_POINT" ]; then


      # ================================================================
      # Create a mountpoint for the external filesystem.
      # ================================================================
      mkdir -p "$EXT_MOUNT_POINT"
    fi


    # ==================================================================
    # Check if the external filesystem is not already mounted.
    # ==================================================================
    if [ $(mountpoint "$EXT_MOUNT_POINT" | grep "not" | wc -l) == 1 ]; then


      # ================================================================
      # Display some information.
      # ================================================================
      echo
      echo "Mount the external filesystem..."


      # ================================================================
      # Mount the external filesystem.
      # ================================================================
      sudo mount -t cifs -o username="$EXT_USERNAME",password="$EXT_PASSWORD",uid=1000,gid=1000,iocharset="utf8",rw,dir_mode=0777,file_mode=0666 //"$EXT_IP"/"$EXT_SHARE" "$EXT_MOUNT_POINT"
    fi


    ####################################################################
    ####################################################################
    #########    E X T E R N A L   B A C K U P   ( N A S )    ##########
    ####################################################################
    ####################################################################


    # ==================================================================
    # Check if the external filesystem is mounted.
    # ==================================================================
    if [ ! $(mountpoint "$EXT_MOUNT_POINT" | grep "not" | wc -l) == 1 ]; then


      # ================================================================
      # Check if the new backup archive does exist.
      # ================================================================
      if [ -f "$BACKUP_PATH" ]; then


        # ==============================================================
        # Display some information.
        # ==============================================================
        echo
        echo "Copy backup to external filesystem..."


        # ==============================================================
        # Copy the actual backup to the external filesystem.
        # ==============================================================
        cp $BACKUP_PATH $EXT_MOUNT_POINT/$BACKUP_FULLNAME
      fi
    fi


    # ==================================================================
    # Show the existing external backup files.
    # ==================================================================
    echo
    echo Existing external backups:
    echo --------------------------
    find $EXT_MOUNT_POINT/ -name *_$BACKUP_BASENAME


    ####################################################################
    ####################################################################
    #########      U N M O U N T   E X T E R N A L   F S      ##########
    ####################################################################
    ####################################################################


    # ==================================================================
    # Check if the NAS share is mounted.
    # ==================================================================
    if [ ! $(mountpoint "$EXT_MOUNT_POINT" | grep "not" | wc -l) == 1 ]; then


      # ================================================================
      # Display some information.
      # ================================================================
      echo
      echo "Unmount the external filesystem..."


      # ================================================================
      # Unmount the external filesystem.
      # ================================================================
      sudo umount $EXT_MOUNT_POINT
    fi
  fi


  ######################################################################
  ######################################################################
  #################   B A C K U P   R O T A T I O N   ##################
  ######################################################################
  ######################################################################


  # ====================================================================
  # Display some information.
  # ====================================================================
  echo
  echo "Processing backup rotation..."


  # ====================================================================
  # Lower the new loop counter.
  # ====================================================================
  BACKUP_STORE=`expr $BACKUP_STORE - 1`


  # ====================================================================
  # Loop through all the existing backup files for moving them to a
  # higher position.
  # ====================================================================
  for(( position=$BACKUP_STORE; position>=1; position-- )); do


    # ==================================================================
    # Check if in the actual position exist a backup file.
    # ==================================================================
    if [ -f $BACKUP_TARGET/${position}_$BACKUP_BASENAME ]; then


      # ================================================================
      # Specify the next higher position number.
      # ================================================================
      NEXT=`expr $position + 1`


      # ================================================================
      # Move the actual backup file to a higher position.
      # ================================================================
      mv -f $BACKUP_TARGET/${position}_$BACKUP_BASENAME $BACKUP_TARGET/${NEXT}_$BACKUP_BASENAME
    fi
  done


  # ====================================================================
  # Save the actual backup file to the first position.
  # ====================================================================
  mv -f $BACKUP_PATH $BACKUP_TARGET/1_$BACKUP_BASENAME


  # ====================================================================
  # Show the existing local backup files.
  # ====================================================================
  echo
  echo Existing local backups:
  echo -----------------------
  find $BACKUP_TARGET/ -name *_$BACKUP_BASENAME


  ######################################################################
  ######################################################################
  #############   E - M A I L   N O T I F I C A T I O N   ##############
  ######################################################################
  ######################################################################


  # ====================================================================
  # Check if email notifications should be sent.
  # ====================================================================
  if [ "$EMAIL_NOTIFICATIONS" == "yes" ]; then


    # ==================================================================
    # Check if the backup file exist.
    # ==================================================================
    if [ -f $BACKUP_TARGET/1_$BACKUP_BASENAME ]; then


      # ================================================================
      # Calculate the filesize in bytes.
      # ================================================================
      FILESIZEB=$(stat -c%s "$BACKUP_TARGET/1_$BACKUP_BASENAME")


      # ================================================================
      # Specify the number/unit.
      # ================================================================
      NUMBER=1
      UNIT="B"


      # ================================================================
      # Check if the filesize >= 1 kilobyte.
      # ================================================================
      if [ "$FILESIZEB" -ge "1024" ]; then


        # ==============================================================
        # Specify the number/unit.
        # ==============================================================
        NUMBER=1024
        UNIT="KB"


        # ==============================================================
        # Check if the filesize >= 1 megabyte.
        # ==============================================================
        if [ "$FILESIZEB" -ge "1048576" ]; then


          # ============================================================
          # Specify the number/unit.
          # ============================================================
          NUMBER=1048576
          UNIT="MB"


          # ============================================================
          # Check if the filesize >= 1 gigabyte.
          # ============================================================
          if [ "$FILESIZEB" -ge "1073741824" ]; then


            # ==========================================================
            # Specify the number/unit.
            # ==========================================================
            NUMBER=1073741824
            UNIT="GB"
          fi
        fi
      fi


      # ================================================================
      # Calculate the filesize.
      # ================================================================
      DISPLAY_FILE_SIZE=`echo "scale=2;$FILESIZEB / $NUMBER" | bc`


      # ================================================================
      # Specify the backup end time.
      # ================================================================
      BACKUP_END=`date +%s`


      # ================================================================
      # Calculate the backup time.
      # ================================================================
      BACKUP_TIME=$((BACKUP_END-START))


      # ================================================================
      # Specify more email details.
      # ================================================================
      EMAIL_SUBJECT="$E_DATE / $E_TIME | Backup successful: '$BACKUP_FULLNAME'"
      EMAIL_MESSAGE="Start date: $E_DATE\nStart time: $E_TIME\nHostname: $SCRIPT_HOST\nBackup time: $BACKUP_TIME s\nBackup size: $DISPLAY_FILE_SIZE $UNIT\nNewest backup: $BACKUP_LINK/1_$BACKUP_BASENAME"


      # ================================================================
      # Display some information.
      # ================================================================
      echo
      echo "Sending a backup notification via email..."


      # ================================================================
      # Send a notification via email.
      # ================================================================
      $SENDEMAIL -o tls=$EMAIL_TLS -f $EMAIL_SENDER -t $EMAIL_RECEIVER -s $EMAIL_HOSTNAME:$EMAIL_PORT -xu $EMAIL_USERNAME -xp $EMAIL_PASSWORD -u "$EMAIL_SUBJECT" -m "$EMAIL_MESSAGE" -o message-charset=utf-8


    # ==================================================================
    # The backup file does not exist.
    # ==================================================================
    else


      # ================================================================
      # Specify more email details.
      # ================================================================
      EMAIL_SUBJECT="$E_DATE / $E_TIME | Backup '$BACKUP_FULLNAME' failed!"
      EMAIL_MESSAGE="$E_DATE / $E_TIME | The backup '$BACKUP_FULLNAME' has failed!"


      # ================================================================
      # Display an error message.
      # ================================================================
      echo
      echo "Sending an error notification via email..."


      # ================================================================
      # Send an error message via email.
      # ================================================================
      $SENDEMAIL -o tls=$EMAIL_TLS -f $EMAIL_SENDER -t $EMAIL_RECEIVER -s $EMAIL_HOSTNAME:$EMAIL_PORT -xu $EMAIL_USERNAME -xp $EMAIL_PASSWORD -u $EMAIL_SUBJECT -m $EMAIL_MESSAGE -o message-charset=utf-8
    fi
  fi
fi


########################################################################
########################################################################
###########   D I S P L A Y   E X E C U T I O N   T I M E   ############
########################################################################
########################################################################


# ======================================================================
# Specify the script ending time.
# ======================================================================
SCRIPT_END=`date +%s`


# ======================================================================
# Calculate the script runtime.
# ======================================================================
SCRIPT_TIME=$((SCRIPT_END-START))


# ======================================================================
# Display the script runtime.
# ======================================================================
echo
echo "Total backup time: $SCRIPT_TIME seconds"
echo
