# FolderBackup Script - README #
---

### Overview ###

The **FolderBackup** script (Bash) copies the specified directories into a compressed tar file (tar.gz) and sends an email notification with a download link for the backup file. Now the script can also mount a NAS drive/directory (CIFS) and copy the backup files to this drive/directory.


### Setup ###

* Install the software packages **sendemail**, **libnet-ssleay-perl** and **libio-socket-ssl-perl**.
* You can use the command: **apt-get install sendemail libnet-ssleay-perl libio-socket-ssl-perl**
* Copy the backup script **folder_backup.sh** to your computer.
* Make the backup script executable: **chmod +x folder_backup.sh**.
* Edit the configuration part of the **folder_backup.sh** script.
* Open the two firewall ports needed for file access (**445**) and for sending email (**587**).
* Be sure to NOT open the port **445** to the WAN or another untrusted network!!!!
* Start the backup script from the command prompt: **./folder_backup.sh**
* Check if the backups are created and a notification was sent by email.

### Support ###

This is a free tool and support is not included and guaranteed. Nevertheless I will try to answer all your questions if possible. So write to my email address **biegel[at]gmx.ch** if you have a question :-)

### License ###

The **FolderBackup** script is licensed under the [**MIT License (Expat)**](https://pb-soft.com/resources/mit_license/license.html) which is published on the official site of the [**Open Source Initiative**](http://opensource.org/licenses/MIT).
